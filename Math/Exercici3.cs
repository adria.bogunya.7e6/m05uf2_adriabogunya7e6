﻿using System;

namespace Exercici3
{
    class Exercici3
    {
        static void Main(string[] args)
        {
            var result = 0.0;
            for (int i = 0; i <= 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                if (result < 0)
                {
                    result += (result * result);
                }

                result += 20.2;
            }

            Console.WriteLine("el resultat és $" +result);

        }
    }
}
// Quin valor té result quan la i == 1000? 30,76
// El valor result és mai major que 110? si (111.56)
// I que 120? No
