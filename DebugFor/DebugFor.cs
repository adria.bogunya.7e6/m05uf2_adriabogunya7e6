﻿using System;
using System.Numerics;

namespace DebugFor
{
    class DebugFor
    {
        static void Main()
        {
            BigInteger numero1 = 548745184;
            BigInteger numero2 = 25145;
            BigInteger result = 0;
            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }

            Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);

        }
    }
}
